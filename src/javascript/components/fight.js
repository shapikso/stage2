import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let pressed = new Set();

    const leftFighter = initializeFighter(firstFighter, 'left');
    const rightFighter = initializeFighter(secondFighter, 'right');

    function keyDown(event) {
      if (!event.repeat) {
        pressed.add(event.code);
      }

      if (isLeftPlayerAttacking(event, pressed)) {
        hit(leftFighter, rightFighter);
      }

      if (isRightPlayerAttacking(event, pressed)) {
        hit(rightFighter, leftFighter);
      }

      if (isLeftPlayerCriticalAttacking(pressed)) {
        const result = makeCriticalHit(leftFighter, rightFighter);
        if (!result) {
          controls.PlayerOneCriticalHitCombination.forEach((code) => pressed.delete(code));
        }
      }

      if (isRightPlayerCriticalAttacking(pressed)) {
        const result = makeCriticalHit(rightFighter, leftFighter);
        if (!result) {
          controls.PlayerTwoCriticalHitCombination.forEach((code) => pressed.delete(code));
        }
      }

      if (rightFighter.health <= 0) {
        setWinner(firstFighter);
      }
      if (leftFighter.health <= 0) {
        setWinner(secondFighter);
      }
    }

    function keyUp(event) {
      pressed.delete(event.code);
    }

    document.addEventListener('keydown', keyDown);
    document.addEventListener('keyup', keyUp);

    function setWinner(winner) {
      document.removeEventListener('keydown', keyDown);
      document.removeEventListener('keyup', keyUp);

      resolve(winner);
    }
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const attack = fighter.attack;
  const criticalHitChance = 1 + Math.random();
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const defense = fighter.defense;
  const dodgeChance = 1 + Math.random();
  const power = defense * dodgeChance;
  return power;
}

export function hit(attacker, defender) {
  defender.health -= getDamage(attacker, defender);
  changeHealthIndicator(defender)
  return true;
}

export function initializeFighter(fighter, position) {
  return {
    ...fighter,
    fullHealth: fighter.health,
    coolDownCrit: new Date() - 10000,
    healthBar: document.getElementById(`${position}-fighter-indicator`),
  };
}

export function isLeftPlayerAttacking(event, pressed) {
  return event.code === controls.PlayerOneAttack &&
          isCanAttacking(event, pressed)
}

export function isRightPlayerAttacking(event, pressed) {
  return event.code === controls.PlayerTwoAttack &&
          isCanAttacking(event, pressed)
}

export function isCanAttacking(event, pressed) {
  return !pressed.has(controls.PlayerOneBlock) &&
            !pressed.has(controls.PlayerTwoBlock) &&
            !event.repeat
}

export function isLeftPlayerCriticalAttacking(pressed) {
  return controls.PlayerOneCriticalHitCombination.every((code) => pressed.has(code))
}

export function isRightPlayerCriticalAttacking(pressed) {
  return controls.PlayerTwoCriticalHitCombination.every((code) => pressed.has(code))
}

export function makeCriticalHit(attacker, defender) {
  const dateNow = new Date();
  if (!(dateNow - attacker.coolDownCrit > 10000)) {
    return false;
  }

  defender.health -= attacker.attack * 2;
  attacker.coolDownCrit = dateNow;
  changeHealthIndicator(defender)
  return true;
}

export function changeHealthIndicator(defender) {
  const percentHealth = defender.health > 0 ? Math.floor((defender.health / defender.fullHealth) * 100) : 0;
  defender.healthBar.style.width = `${percentHealth}%`;
}
