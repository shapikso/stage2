import { showModal } from './modal'
import { createFighterImage } from '../fighterPreview';
import { createFighters } from '../fightersView';
import { fighterService } from '../../services/fightersService';

export function showWinnerModal(fighter) {
  const imageElement = createFighterImage(fighter);
  const name = fighter.name.toUpperCase()
  const modalElement = {
    title: `WINNER: ${name}!`,
    bodyElement: imageElement,
    onClose,
  };
  
  showModal(modalElement); 
}

const onClose = () => {
  const arena = document.getElementsByClassName('arena___root')[0];
  arena?.remove();
  
  const root = document.getElementById('root');
  fighterService.getFighters().then(fighters => {
    const fightersElement = createFighters(fighters);
    root.appendChild(fightersElement);
  });
}
