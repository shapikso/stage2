import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';

  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`, 
  });
  if(fighter) {
  const { name, health, attack, defense } = fighter;
  const showFighterInfo = createElement({
    tagName: 'div', 
    className: `fighters___info___${position}`, 
  });
  const image = createFighterImage(fighter);
  const showFighterName = createElement({
    tagName: 'div', 
    className: `fighters___text___${position}`, 
  });
  showFighterInfo.textContent = `Health: ${health} Attack: ${attack} Defense: ${defense}`;
  showFighterName.textContent = `${name}`;
  fighterElement.append(showFighterName, image, showFighterInfo);
  }
  // todo: show fighter info (image, name, health, attack, defense.)
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}